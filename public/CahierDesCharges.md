# CAHIER DES CHARGES

Par **Mohamed Wael Zouaoui**, **Jean-Baptiste Caillaud** et **Anthony Briot**

Ludus Académie 2019/2020

- Nom du projet : Ushi Project
- Lien vers le projet : [https://gitlab.com/yShimoka/ushi](https://gitlab.com/yShimoka/ushi)
- Email : jb.caillaud@ludus-academie.com / mw.zouaoui@ludus-academie.com / a.briot@ludus-academie.com
---
## SOMMAIRE



### <a href="#a"> A. PRÉSENTATION DE L'EQUIPE
#### <a href="#a1">A. 1. LES OBJECTIFS DE L'APPLICATION
#### <a href="#a2">A. 2. LES CIBLES
#### <a href="#a3">A. 3. LES OBJECTIFS QUANTITATIFS
#### <a href="#a4">A. 4. LE TYPE D'APPLICATION
#### <a href="#a5">A. 5. L'EQUIPEMENT DE NOS CIBLES
#### <a href="#a6">A. 6. PÉRIMÈTRE DU PROJET
---
### <a href="#b">B. GRAPHISME ET ERGONOMIE
#### <a href="#b1">B. 1. LA CHARTE GRAPHIQUE
#### <a href="#b2">B. 2. WIREFRAME ET MAQUETTAGE
---
### <a href="#c">C. SPÉCIFICITÉS ET LIVRABLES
#### <a href="#c1">C. 1. LE CONTENU DE NOTRE APPLICATION
#### <a href="#c2">C. 2. CONTRAINTES TECHNIQUES
#### <a href="#c3">C. 3. LES LIVRABLES
#### <a href="#c4">C. 4. LE PLANNING

---
### <p id="a"/>A. PRÉSENTATION DE L'EQUIPE
>- Activité principale : création de jeu VR
>- Nombre de membres : 3
>- Date de début de projet : mardi 8 octobre 2019
>- Date de rendu :  mardi 19 novembre 2019
>
> Nous développons actuellement un jeu en VR sur Unreal  Engine 4 sur la version 4.22. Nous souhaitons créer un jeu sandbox où le joueur devra gérer une île sur laquelle des animaux arrivent en fonction de leur besoin et il devra apprendre à les satisfaire s'il souhaite les faire rester.

#### <p id="a1"/>A. 1. LES OBJECTIFS DE L'APPLICATION
> Notre objectif est de créer un jeu permettant aux gens de se détendre après une dure journée de travail en s'occupant de petits animaux dans une ambiance douce et relaxante.

#### <p id="a2"/>A. 2. LES CIBLES
> Nous visons un public professionnel en début de carrière ayant les moyens d'investir dans la VR et ayant envie de se divertir et de se détendre dans un petit jardin secret. Le profil type de nos prospects corresponds à : une femme de 25-30 ans, dans une entreprise de développement informatique, ancienne joueuse de jeux tel qu'*Animal Crossing*, *Viva Pinata*, *Harvest Moon*, *My Time at Portia*, ou encore *Stardew Valley*. 

#### <p id="a3"/>A. 3. LES OBJECTIFS QUANTITATIFS
> Etant donné qu'il s'agit d'un jeu réalisé dans le cadre d'un projet scolaire, nous ne nous attendons pas à le vendre sur une plateforme de jeux. Toutefois, si nous le mettions sur *Steam* nous pourrions espérer un millier de téléchargements (voir plus) car c'est un style de jeu relativement populaire, et celui-ci se démarquera des autres du fait qu'il soit en VR et qu'il ai son charme grâce à ses assets en low poly et aux couleurs pastels.

#### <p id="a4"/>A. 4. LE TYPE D'APPLICATION
>- Jeu VR / Sandbox
>- Jeu de Gestion dans lequel le joueur doit gérer la venue d'animaux sur une île flottante

#### <p id="a5"/>A. 5. L'EQUIPEMENT DE NOS CIBLES
> L'application restera sur PC mais devra être compatible Windows et Mac et fonctionner sur tous types de casque VR. Il sera donc nécessaire pour notre cible de posséder un casque VR pour pouvoir y jouer.

#### <p id="a6"/>A. 6. PÉRIMÈTRE DU PROJET
> Nous souhaiterions proposé une version du jeu en français et en anglais. Le jeu ne disposerait d'aucun achat in-app lors de sa première release mais pourrait par la suite proposé des DLC comprenant de nouveaux animaux ainsi que de nouveaux biomes. Le jeu devra être jouable en mode hors connexion.
---
### <p id="b"/>B. GRAPHISME ET ERGONOMIE
#### <p id="b1"/>B. 1. LA CHARTE GRAPHIQUE
> Notre jeu sera composé de couleurs pastels, pour que cela corresponde à l'ambiance que nous recherchons. Nous utiliserons également des assets en low poly qui permettront de conserver la forme de base des animaux et des bâtiments et nous permettra d'avoir un jeu plus fluide. Cela donnera également un style rétro à notre jeu tout en étant sur une technologie récente à savoir la VR, ce qui lui donnera un certain charme.
> Nous utiliserons un police proche de celles des cartoons comme la *Splash* [https://www.dafont.com/fr/splatch.font](https://www.dafont.com/fr/splatch.font) qui se mariera très bien avec des graphismes low-poly et des couleurs pastels.
> Voici un exemple des couleurs que nous souhaiterions utilisées pour donner un aspect innocent et calme à notre jeu : [https://play.google.com/store/apps/details?id=com.ustwo.monumentvalley2&hl=fr](https://play.google.com/store/apps/details?id=com.ustwo.monumentvalley2&hl=fr)
> Ainsi qu'un exemple des assets en low poly qui donnent un côté cartoon à notre application :[https://store.steampowered.com/app/508440/Totally_Accurate_Battle_Simulator/](https://store.steampowered.com/app/508440/Totally_Accurate_Battle_Simulator/)

#### <p id="b2"/>B. 2. WIREFRAME ET MAQUETTAGE
> Exemple de rendu de l'île : [https://gamebias.files.wordpress.com/2014/10/mountain.jpg](https://gamebias.files.wordpress.com/2014/10/mountain.jpg)
> Exemple de HUD : https://www.roadtovr.com/wp-content/uploads/2015/04/google-acquires-tilt-brush.jpg
> Exemple de menu principal : https://www.mobygames.com/images/shots/l/200087-viva-pinata-xbox-360-screenshot-title-screen.jpg
---
### <p id="c"/>C. SPÉCIFICITÉS ET LIVRABLES
#### <p id="c1"/>C. 1. LE CONTENU DE NOTRE APPLICATION
> Liste des assets à créer :
 - Asset low poly fbx/obj : poule
 - Asset low poly fbx/obj : renard
 - Asset low poly fbx/obj : corbeau
 - Asset low poly fbx/obj : tortue
 - Asset low poly fbx/obj : loup
 - Asset low poly fbx/obj : crabe
 - Asset low poly fbx/obj : ours
 - Asset low poly fbx/obj : manchot
 - Asset low poly fbx/obj : poisson
 - Asset low poly fbx/obj : écureuil
 - Asset low poly fbx/obj : arbres
 - Asset low poly fbx/obj : rochers
 - Asset low poly fbx/obj : bâtiments
 - Asset low poly fbx/obj : barrières

> Liste des fonctions à créer :
 - Création de la carte
	 - Fonction VOXEL pour le découpage de l'île en cubes
	 - Fonction de génération aléatoire de l'île
- IA des animaux
	-  Venue sur l'île en fonction des biomes présents
	- Départ en fonction des biomes présents
	- Déplacement au sein d'un biome
	- Attaque d'une autre espèce
	- Collision avec les barrières
- Interactions avec le HUD
	- PawnVR
	- Roue des possibilités
	- Sélection d'une possibilité avec raycast / surbrillance 
	- Dé-sélection
- Interactions avec le terrain
	- Changer le type de biome
	- Modeler le terrain (vers le haut et vers le bas)
	- Placer des bâtiments (Rotation, Placement, Suppression, Modification)
	- Placer des ornements (Rotation, Placement, Suppression, Modification)
- Interactions avec l'île
	- Zoom / Dé-zoom
	- Déplacement (Multi-directions)
	- Rotation

#### <p id="c2"/>C. 2. CONTRAINTES TECHNIQUES
- Posséder un casque VR
- Formation à l'utilisation de la VR
- Formation au développement de la VR

#### <p id="c3"/>C. 3. LES LIVRABLES
>Nous comptons 12h de travail par semaine par personne, étant donné que le projet dure 6 semaine, nous comptons : 12h de travail par semaine X 6 semaines X 3 personnes X 20€/h pour un total de 12 X 6 X 3 X 20=4320€.

- 29/10/2019 - Alpha :
	- Création de la carte (VOXEL + génération aléatoire)
	- Cahier des charges + Game Design Document
	- PawnVR + Roue des possibilités
	- Préparation des assets
- 05/11/2019 - Beta :
	- Interactions avec le HUD ( Sélection avec raycast et surbrillance + dé-sélection)
	- IA des animaux (Venue + Départ + Déplacement)
	- Interactions avec le terrain ( Changement de biome + Modelage du terrain)
	
- 12/11/2019 - Release Version :
	- Interactions avec le terrain (Bâtiments + Ornements)
	- IA des animaux (Attaque d'autres espèces + Collision avec les barrières)
	- Interactions avec l'île (Zoom/Dé-zoom + Déplacement + Rotation)

- 19/11/2019 - Release Version Debug :
	- Intégrations de nouveaux animaux / bâtiments / ornements

#### <p id="c4"/>C. 4. LE PLANNING
> Voir GitKraken Glo