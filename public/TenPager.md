
# TEN PAGER

Par **Mohamed Wael Zouaoui**, **Jean-Baptiste Caillaud** et **Anthony Briot**

**Ludus Académie 2019/2020**

- Nom du projet : Ushi Project
- Lien vers le projet : [https://gitlab.com/yShimoka/ushi](https://gitlab.com/yShimoka/ushi)
- Email : jb.caillaud@ludus-academie.com / mw.zouaoui@ludus-academie.com / a.briot@ludus-academie.com


---
## SOMMAIRE



## <a href="#a"> INFORMATIONS GÉNÉRALES
### <a href="#b">VALEURS ET INTENTIONS
### <a href="#c">RÉFÉRENCES
### <a href="#d">PITCH
### <a href="#e"> DIRECTION ARTISTIQUE
#### <a href="#f">GRAPHISME
#### <a href="#g">AUDIO
---
## <a href="h">NARRATION
### <a href="#i">SCÉNARIO
### <a href="#j">WORLD VIEW
### <a href="#k">PRÉSENTATION DU PERSONNAGE
### <a href="#l">CAPACITÉS
### <a href="#m">CONTRÔLES

---
## <a href="#n">GAMEPLAY
## <a href="#o">GAME LOOPS
## <a href="#p">MECHANICS & LEVEL ELEMENTS
### <a href="#q">HAZARDS
### <a href="#r">COLLECTIBLES
## <a href="#s">EXPÉRIENCE
### <a href="#t">VIEW
### <a href="#w">INTERFACE
### <a href="#x">FLOWCHART


---
##  <p id="a"/>INFORMATIONS GÉNÉRALES
-	Nom du projet : Ushi Project
-	Genre : Sandbox / Gestion 
-	Caméra / View : 3D FPS ( VR )
-	Solo / Multi : Solo 
-	Plateforme : PC / Mac ( VR )
-	Target / Cible : Professionnel en début de carrière intéressé dans le développement de jeu VR, voulant se détendre dans son jardin virtuel après le travail.
-	Business Model : Retail + Achat In-App
-	PEGI : <img src="https://pegi.info/sites/default/files/inline-images/age-7-black.jpg" title="pegi" alt="PEGI" width="30px"/>
### <p id="b"/>VALEURS ET INTENTIONS
>L'objectif de notre jeu est de permettre au joueur de se détendre après une dure journée, le jeu le récompensera celui-ci s'ils comprends les différentes mécaniques de terrains liées aux animaux ce qui lui permettra d'avoir la faune qu'il souhaite sur son île.
>Notre jeu a pour but d'être gratifié de son ambiance calme, relaxante, qui permet au joueur d'oublier le reste et de se concentrer sur son petit jardin secret.

### <p id="c"/>RÉFÉRENCES
- Elysium /VR sand /tiny wheels /kitten'd /garden of the sea /deisim / fujiii (à compléter)
- Block'hood : Pour le principe d'île flottante à taille limitée sur laquelle on se développe de la manière qu'on souhaite.
- Tilt Brush : Interface utilisateur similaire à ce que l'on souhaite réaliser.
- Viva Pinata : Des animaux qui viennent sur l'île en fonction des ressources.
- TABS (Totally Acurate Battle Simulator) : Visuels en low poly.
- Cube World : Visuels en low poly ainsi que le principe du VOXEL qui consiste à créer des cubes (dans notre cas et dans Cube World) et d'y stocker des informations comme la couleur qui correspond au type de terrain.
- Moutain : Aspect visuel de l'île flottante.
### <p id="d"/>PITCH
>Le joueur se retrouve plongé dans un univers dans lequel il se trouve devant une île qu'il peut modifier à sa guise afin de faire venir des animaux de toutes sortes et les faire rester. Il pourra modeler le terrain et changer le type de biomes (eau, sable, montagne, prairie). 
### <p id="e"/>DIRECTION ARTISTIQUE

#### <p id="f"/>GRAPHISME
> Notre jeu est composé de couleurs pastels, pour que cela corresponde à l'ambiance que nous recherchons. Nous utilisons également des assets en low poly qui permettront de conserver la forme de base des animaux et des bâtiments et nous permettra d'avoir un jeu plus fluide. Cela donne également un style rétro à notre jeu tout en étant sur une technologie récente à savoir la VR, ce qui lui donnera un certain charme.
#### <p id="g"/> AUDIO
> Nous utilisons des musiques douces pour appuyer sur la paisibilité et la tranquillité qui règne au sein de notre île. Dès qu'un prédateur apparaîtra pour l'un des animaux, un son marquant sera joué et une musique inspirant la panique prendra place jusqu'au moment où le prédateur s'en ira (ref : Sonic the Hedgehog 2 - Underwater).

## <p id="h"/>NARRATION

### <p id="i"/> SCÉNARIO
> Le joueur se retrouve sans raison apparente devant une île qu'il peut contrôler, il est comme le dieu de celle-ci. Il se rends ensuite compte que des animaux arrivent sur son île, qu'il peut les faire partir s'il ne réponds pas à leurs besoins et qu'il peut à l'inverse, les faire rester si ceux-ci se sentent à l'aise, à lui de faire les bons compromis pour avoir la faune qu'il souhaite conserver sur son île.
### <p id="j"/>WORLD VIEW
![Moodboard](https://lh3.googleusercontent.com/7kjy2m1v1hEqjt6iZXSQ6ofVl4cq2Zwol4aLLFdC2PwBKHihn-2o42CCqlTg-62ASdeTr4JusHWF "1")
### <p id="k"/>PRÉSENTATION DU PERSONNAGE
>L'avatar est représenté par le joueur, c'est lui qui est directement plongé au sein même du jeu. Il est très grand par rapport à l'île, il peut également changer l'échelle afin de se retrouver à la taille des animaux pour les observer ou pour placer des ornements plus aisément.
### <p id="l"/>CAPACITÉS
>L'avatar ne dispose que de capacités influentes sur le terrain de l'île, il peut la modeler, placer ou supprimer des ornements et modifier les biomes à sa guise mais il ne peut pas interagir directement avec les animaux qui la peuplent.
- Zoomer
- Dé-zoomer
- Faire pivoter l'île
- Changer le biome
- Changer la hauteur du terrain (monter - descendre )
- Placer un élément d'ornement ou un bâtiment
- Déplacer un élément d'ornement ou un bâtiment
- Supprimer un élément d'ornement ou un bâtiment
### <p id="m"/>CONTRÔLES
![Résultat de recherche d'images pour "vr controller input schema"](https://forums.unrealengine.com/attachment.php?attachmentid=104324&stc=1)
- Zoomer : Maintien de la touche 7 sur les 2 manettes et mouvement d'écartement des bras.
- Dé-zoomer : Maintien de la touche 7 sur les 2 manettes et mouvement de rassemblement des bras.
- Faire pivoter l'île : Maintien de la touche 7 sur une manette et mouvement vers la gauche ou la droite en fonction de la rotation souhaitée.
- Changer le biome : Sélection du changement de biome sur l'interface accrochée au controller gauche à l'aide du raycast et de la pression de la touche 3. Puis modification d'intensité et d'autres paramètres à l'aide des touches 2-3-4-5.
- Changer la hauteur du terrain (monter - descendre ) : Sélection du changement de terrain sur l'interface accrochée au controller gauche à l'aide du raycast et de la pression de la touche 3. Puis modification d'intensité et d'autres paramètres à l'aide des touches 2-3-4-5.
- Placer un élément d'ornement ou un bâtiment : Sélection placement de bâtiment ou d'ornement sur l'interface accrochée au controller gauche à l'aide du raycast et de la pression de la touche 3. Les touches 2-4-5 serviront à sélectionner si le joueur souhaite Placer, Déplacer, ou Supprimer un bâtiment ou un élément d'ornement.
- Déplacer un élément d'ornement ou un bâtiment : De même que pour le point précédent.
- Supprimer un élément d'ornement ou un bâtiment : De même que pour les 2 points précédents.

## <p id="n"/>GAMEPLAY
>  Ushi Project est un jeu de simulation de vie à la première personne dans lequel le joueur modèle et s'occupe d'une île. Le joueur utilise des outils de modelage, pour créer l'environnement qu'il souhaite offrir à sa population, modifier les biomes, pour permettre aux animaux qu'il souhaite avoir de venir et de rester. 
>  Lorsque certaines conditions sont remplies, une nouvelle espèce apparaît sur l'île. Après avoir pris ses marques et si l'environnement lui correspond toujours, l'espèce deviendra résidente de l'île. 
>  Certains animaux sont agressifs envers d'autres animaux, s'ils y arrivent, il peuvent les manger. Ces mêmes animaux peuvent rester sur l'île uniquement s'ils ont de quoi se nourrir et du coup il sera impératif pour le joueur s'il souhaite conserver ces espèces d'avoir d'autres animaux pour les nourrir.
>   Le joueur peut toutefois protéger ses animaux en construisant ses barrières.
>    Il existe une chaîne alimentaire, avec un certain nombre d'espèces différentes dont une ou deux autres sont considérées comme des proies. Lorsque les animaux visitent l'île, ils doivent satisfaire à leurs exigences. Une fois que les animaux sont résident, ils ne peuvent manger d'autres animaux que si le joueur le permet.
*"Mieux vaut mettre ses exigences de côté, prendre son temps, rester calme et se laisser embarquer par l'aspect plus bac à sable."*

## <p id="o"/>GAME LOOPS
- Modifier un biome -> Nouvelle espèce apparaît -> Nouvelle espèce résidente
- Modifier un biome en prairie + posséder une poule -> Renard apparaît et mange une poule -> Nouvelle espèce résidente (renard)
- Modifier un biome en prairie + placer des barricades autour d'une poule -> Renard apparaît mais ne peut manger une poule -> Pas de nouvelle espèce résidente (renard)
- Modifier 2 biomes pour avoir un biome neige et eau collés l'un à l'autre -> Manchot apparaît -> Nouvelle espèce résidente (manchot)
## <p id="p"/>MECHANICS & LEVEL ELEMENTS
> Le joueur peut sélectionner :
- Les éléments présents sur l'île :
	- Les bâtiments
	- Les ornements
	- Les barricades
- Les éléments sur l'interface :
	- Modification de terrain
	- Modification de biome
	- Gestion de bâtiments
	- Gestion d'ornements
- L'île
	- Zoomer
	- Dé-zoomer
	- Pivoter
### <p id="q"/>HAZARDS
> Il n'y a ni minor hazards, qui causerait des dommages à notre personnage principale, ou qui le ralentirait par exemple, ni major hazards qui causerait un game over.
### <p id="r"/>COLLECTIBLES
> Les seuls éléments que l'on pourrait qualifier de collectibles seront les animaux qui viendront sur l'île et qui serviront de rewards aux joueurs ayant réussi à les faire venir et rester.

## <p id="s"/>EXPÉRIENCE
### <p id="t"/>VIEW
![Résultat de recherche d'images pour "mountain game"](https://gamebias.files.wordpress.com/2014/10/mountain.jpg)
*Exemple de la caméra du jeu.*
### <p id="w"/>INTERFACE
![Résultat de recherche d'images pour "tilt brush hud"](https://secureservercdn.net/50.62.175.49/155.73c.myftpupload.com/wp-content/uploads/2015/11/google-acquires-tilt-brush.jpg)
*Exemple d'interface.*
### <p id="x"/>FLOWCHART
![flowchart](https://lh3.googleusercontent.com/kGTcHHtjrOcdsY0cg4QGzTq0GgHsQ_NVawr-s6zJxmq8TynWD4L3FnpiuI2eBaInf7KsQfVbz2NA)



