# CHANGELOG - GAME DESIGN

Par **Mohamed Wael Zouaoui**, **Jean-Baptiste Caillaud** et **Anthony Briot**

**Ludus Académie 2019/2020**

- Nom du projet : Ushi Project
- Lien vers le projet : [https://gitlab.com/yShimoka/ushi](https://gitlab.com/yShimoka/ushi)
- Email : jb.caillaud@ludus-academie.com / mw.zouaoui@ludus-academie.com / a.briot@ludus-academie.com
---

## UPDATE
### [1.3.0]17/11/19
#### Added 
- Complétion Ten Pager (Anthony)
	- Valeurs et Intention
	- World view / moodboard
	- Capacités / Contrôles
	- Game Loops
	- Mechanics & level elements
	- Hazards
	- Collectibles
	- View
	- Interface
	- Flowchart
	- Sommaire
- Game Design Document
	- Sommaire

#### Changed
- Ten Pager (Anthony)
	- Gameplay
### [1.2.2]12/11/19
#### Added
- Ten Pager (Anthony et Wael)
	- Gameplay
	- Capacités
	- Audio
	- Contrôles
#### Changed
 - Références

### [1.2.1] 05/10/19
#### Added
- Deuxième jet du ten pager (Anthony)
	- Références
	- Pitch
	- Direction Artistique
	- Scénario
	- Gameplay


### [1.2.0] 04/10/19
#### Added
- Premier jet du ten pager (Wael)
	- Références
	- Pitch
	- Direction Artistique
	- Scénario
	- Gameplay


### [1.1.1] 29/10/19
#### Added 
- Livrables dans le Cahier des Charges
- Tarification Homme/temps de travail
- ChangeLog Développement (Wael - Anthony)
- ChangeLog Game Design (Wael - Anthony)
- Préparation du TenPager (Sommaire et Contenu) (Wael - Anthony)
#### Changed 
- Game Design Document : Changement d'entête
- Game Design Document : Correction fautes d'orthographe / grammaire
- Cahier des Charges : Changement des titres et de l'entête
- Cahier des Charges : Liens permettant d'aller plus vite où l'on souhaite dans le document
### [1.1.0] 22/10/19
#### Added
- Première release Cahier des Charges
	- Présentation de l'équipe
		- Les objectifs de l'application
		- Les cibles
		- Les objectifs quantitatifs
		- Le type d'application
		- L'équipement de nos cibles
		- Périmètre du projet
	- Graphisme et ergonomie
		- La charte graphique
		- Wireframe et maquettage
	- Spécificités et livrables
		- Le contenu de notre application
		- Contraintes techniques
		- Le planning
#### Changed
- Complétion du Game Design Document
	- Game Balancing
		- Biomes et animaux
		- Prédateurs
		- Conditions supplémentaires
	- Gameplay
		- Interactions
		- Bâtiments et ornements
	- Game Flow
		- Menus
		- HUD & Feedback visuel / sonore
	- Annexes

### [1.0.2] 15/10/19
#### Added
- Première release Game Design Document
	- Game Overview
		- Univers
		-	Direction artistique
#### Changed
- Game Overview
	-  Intention
	- Références 
### [1.0.1] 09/10/19
#### Added 
- Game Overview
	- Références
#### Changed
-	Game Overview
	-	Intention
### [1.0.0] 08/10/19
#### Added 
- Game Design Document
	- Formating test 
	- Intention
