# GAME DESIGN DOCUMENT

Par **Mohamed Wael Zouaoui**, **Jean-Baptiste Caillaud** et **Anthony Briot**

**Ludus Académie 2019/2020**

- Nom du projet : Ushi Project
- Lien vers le projet : [https://gitlab.com/yShimoka/ushi](https://gitlab.com/yShimoka/ushi)
- Email : jb.caillaud@ludus-academie.com / mw.zouaoui@ludus-academie.com / a.briot@ludus-academie.com


---
## SOMMAIRE



## <a href="#a"> GAME OVERVIEW
### <a href="#b">INTENTION
### <a href="#c">RÉFÉRENCES
### <a href="#d">UNIVERS
### <a href="#e">DIRECTION ARTISTIQUE
#### <a href="#f">DIRECTION GRAPHIQUE
#### <a href="#g">DIRECTION SONORE
---
## <a href="#h">GAME BALANCING
### <a href="#i">BIOMES ET ANIMAUX
### <a href="#j">PRÉDATEURS
### <a href="#k">CONDITIONS SUPPLÉMENTAIRES
---
## <a href="#l">GAMEPLAY
### <a href="#m">INTERACTIONS
### <a href="#n">BÂTIMENTS / ORNEMENTS
---
## <a href="#o">GAMEFLOW
### <a href="#p">MENUS
### <a href="#q">HUD & FEEDBACK VISUEL / SONORE
---
## <a href="#r">ANNEXES
### <a href="#s">LIENS RÉFÉRENCES


---
## <p id="a"/>GAME OVERVIEW

### <p id="b"/>INTENTION

> L’objectif qui nous a été imposer est de créer un jeu VR, nous avons donc décidé de réaliser un jeu sandbox (bac à sable) dans lequel le joueur devra gérer une île flottante sur laquelle des animaux arriveront en fonction de leurs besoins. Le joueur décidera donc de combler leurs besoins s’il désire qu’un type d’animaux en particulier vienne sur l’île et s’il souhaite le faire rester. Des bâtiments ainsi que des clôtures pourront être placer par le joueur pour améliorer le confort de vie des animaux et donc leur permettre de se développer plus rapidement et d’être à l’abri des prédateurs.
> Le joueur pourra modifier le terrain et placer des bâtiments grâce à une interface similaire à celle de *Tilt Brush* mais également zoomer et dézoomer pour se retrouver à l’échelle 1 :1 avec les animaux qui se trouvent sur son île ou l’avoir devant lui pour simplifier ses interactions.

### <p id="c"/>RÉFÉRENCES

- Block'hood : Pour le principe d'île flottante à taille limitée sur laquelle on se développe de la manière qu'on souhaite.
- Tilt Brush : Interface utilisateur similaire à ce que l'on souhaite réaliser.
- Viva Pinata : Des animaux qui viennent sur l'île en fonction des ressources.
- TABS (Totally Acurate Battle Simulator) : Visuels en low poly.
- Cube World : Visuels en low poly ainsi que le principe du VOXEL qui consiste à créer des cubes (dans notre cas et dans Cube World) et d'y stocker des informations comme la couleur qui correspond au type de terrain.
- Moutain : Aspect visuel de l'île flottante.

### <p id="d"/>UNIVERS

> Nous souhaitons proposé au joueur un jeu avec une ambiance douce, calme, voir même relaxante. Et qu'il puisse se développer dans un tel monde selon ses envies. Etant donné qu'il s'agit là d'un jeu sandbox, c'est le joueur qui créera son propre univers et sa propre histoire.

### <p id="e"/>DIRECTION ARTISTIQUE

#### <p id="f"/>DIRECTION GRAPHIQUE

> Notre jeu sera composé de couleurs pastels, pour que cela corresponde à l'ambiance que nous recherchons. Nous utiliserons également des assets en low poly qui permettront de conserver la forme de base des animaux et des bâtiments et nous permettra d'avoir un jeu plus fluide. Cela donnera également un style rétro à notre jeu tout en étant sur une technologie récente à savoir la VR, ce qui lui donnera un certain charme.

#### <p id="g"/>DIRECTION SONORE

> Nous utiliserons des musiques douces pour appuyer sur la paisibilité et la tranquillité qui règne au sein de notre île. Dès qu'un prédateur apparaîtra pour l'un des animaux, un son marquant sera joué et une musique inspirant la panique prendra place jusqu'au moment où le prédateur s'en ira (ref : Sonic the Hedgehog 2 - Underwater).

## <p id="h"/>GAME BALANCING

### <p id="i"/>BIOMES ET ANIMAUX

> Les animaux apparaîtront en fonction des biomes présents sur l'île de joueur, les animaux sont au nombre de 10 et les biomes au nombre de 5, ci-dessous la liste des apparitions des animaux en fonction du biome :

|Biome|Escpèce(s)|
|:------:|:-------:|
|Eau|Poisson|
|Sable|Crabe|
|Herbe|Poule / Renard|
|Roche|Écureuil / Corbeau|
|Neige|Ours|


> Et également les animaux qui nécessitent plusieurs biomes côte à côte pour pouvoir apparaître :

|Biomes|Espèce|
|:--:|:--:|
|Eau + Sable|Tortue|
|Herbe + Roche|Loup|
|Neige + Eau| Manchot|

### <p id="j"/>PRÉDATEURS

> Certains animaux seront des prédateurs, ils pourront également être apprivoisés et gardés sur l'île mais ils se pourraient qu'ils dévorent d'autres animaux du joueur si l'envie leur prends. Ils ne sont que prédateurs envers une espèce en particulier, libre au joueur de décider quel espèce il souhaite conserver sur son île ou effectuer les aménagements nécessaire s'il souhaite conserver 2 espèces dont 1 prédatrice de l'autre. Ci-dessous la liste des prédateurs et des proies :

|Prédateurs|Proies|
|:-----:|:-----:|
|Renard| Poule |
|Ours / Manchot|Poisson|
|Tortue|Crabe|
|Loup / Corbeau|Écureuil|

### <p id="k"/>CONDITIONS SUPPLÉMENTAIRES
> Certains animaux prédateurs ne viendront sur l'île que s'ils ont une ou plusieurs proies à chasser.

## <p id="l"/>GAMEPLAY

### <p id="m"/>INTERACTIONS
> Le joueur possédera, de la même manière que *Tilt brush* une roue de possibilités sur l'une de ses manettes et aura le choix entre plusieurs actions :
> - Modeler le terrain (monter/descendre)
> - Changer la composition du terrain afin de créer des biomes (ex: transformer un bloc herbe en bloc neige)
> - Placer des bâtiments / ornements
> - Supprimer des bâtiments / ornements
> - Déplacer des bâtiments / ornements

> Le joueur pourra déplacer l'île grâce au bouton latéral de la manette. S'il appuie sur les boutons latéraux des 2 manettes simultanées, il pourra effectuer une rotation sur l'île et changer son échelle.


### <p id="n"/>BÂTIMENTS / ORNEMENTS

> Le joueur aura accès à des bâtiments types pour chaque espèce qu'il pourra placer pour améliorer leur confort afin que ceux-ci restent sur l'île. Il pourra aussi placer des barrières afin de créer des enclos pour protéger ses espèces des prédateurs et pour délimiter son île comme il le souhaite. 
> Ci-dessous une liste des bâtiments ainsi que des espèces qui en profitent :

|Bâtiment|Espèce(s)|
|:------:|:-------:|
|Ponton avec filet de nourriture|Poisson|
|Filet de nourriture|Crabe / Manchot|
|Cabane avec mangeoire à grains|Poule|
|Arbre avec maisonnette|Écureuil / Corbeau|
|Cabane Grotte|Ours|
|Cabane avec filet de nourriture suspendu|Renard / Loup|
|Cabane avec ponton|Tortue|
|Clôture|Toutes|

> Ci-dessous des exemples d'ornements : 
> -  Arbres
> - Rochers
> - Fleurs
> - Plantes

## <p id="o"/>GAMEFLOW

### <p id="p"/>MENUS

> Le menu principal sera composé de :
>  - Un bouton jouer qui amènera le joueur directement devant son île
>  - Un bouton options qui lui permettra de gérer les paramètres sonores et la luminosité
>  - Un bouton quitter qui le fera quitter le jeu.

>Il y aura également un menu en jeu qui pourra être sélectionner sur la roue de possibilités et qui ouvrira un menu composé de :
>- Un bouton options similaire à celui du menu principal
>- Un bouton reprendre qui le fera retourner en jeu
>- Un bouton quitter qui enregistrera sa progression et lui fera quitter le jeu

### <p id="q"/>HUD & FEEDBACK VISUEL / SONORE

> Le HUD en jeu sera très épuré, le but étant de plongé le joueur dans le jeu et non pas de l’inondé d'informations. Le seul menu d'interactions dont il disposera sera placé sur l'une de ces mains. Il pourra donc se concentrer pleinement sur son expérience de jeu. Il reste toutefois important d'avoir un certain feedback visuel pour que le jeu reste compréhensible, c'est pourquoi lorsque le joueur souhaitera interagir, nous indiquerons ce qu'il pointe à l'aide d'un raycast et d'une surbrillance. Nous pourrons également savoir si les animaux se portent bien ou non grâce à des émojis. 
> Pour ce qui est du feedback sonore, nous devrons avoir des sons pour chaque choix de notre roue de possibilités lorsqu'on la survole et également lorsque l'on interagit avec le terrain ou que l'on pose un bâtiment. Il faudra également émettre un son avant le moment où un prédateur s'apprête à sauter sur sa proie. Avant que le joueur puisse voir ce qu'il se passe, afin de comprendre comment gérer son île mais également s'il est assez rapide, de pouvoir placer une barricade entre ces deux animaux.

## <p id="r"/>ANNEXES

### <p id="s"/>LIENS RÉFÉRENCES  
- [https://store.steampowered.com/app/416210/Blockhood/](https://store.steampowered.com/app/416210/Blockhood/)
- [https://www.tiltbrush.com/](https://www.tiltbrush.com/)
- [https://fr.wikipedia.org/wiki/Viva_Pi%C3%B1ata](https://fr.wikipedia.org/wiki/Viva_Pi%C3%B1ata)
- [https://store.steampowered.com/app/508440/Totally_Accurate_Battle_Simulator/](https://store.steampowered.com/app/508440/Totally_Accurate_Battle_Simulator/)
- [https://cubeworld.com/](https://cubeworld.com/)
- [https://store.steampowered.com/app/313340/Mountain/](https://store.steampowered.com/app/313340/Mountain/)