# CHANGELOG - DÉVELOPPEMENT

Par **Mohamed Wael Zouaoui**, **Jean-Baptiste Caillaud** et **Anthony Briot**

**Ludus Académie 2019/2020**

- Nom du projet : Ushi Project
- Lien vers le projet : [https://gitlab.com/yShimoka/ushi](https://gitlab.com/yShimoka/ushi)
- Email : jb.caillaud@ludus-academie.com / mw.zouaoui@ludus-academie.com / a.briot@ludus-academie.com


---
## UPDATE

### [1.0.0] 29/10/19

### [1.0.0] 22/10/19

### [1.0.0] 15/10/19
#### Added
#### Changed
#### Removed
#### Fixed
#### Deprecated
