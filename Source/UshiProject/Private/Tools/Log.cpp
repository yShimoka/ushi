//
//  Log.cpp
//  UshiProject_Index
//
//  Created by Jean-Baptiste CAILLAUD on 16/11/2019.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

// Include the log header.
#include "Tools/Log.h"

// Define the log category.
DEFINE_LOG_CATEGORY(UshiLog)
