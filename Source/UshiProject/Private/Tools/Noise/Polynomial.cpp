//
//  Polynomial.cpp
//  UshiProject_Index
//
//  Created by Jean-Baptiste CAILLAUD on 16/11/2019.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

// Include the polynomial header.
#include "Tools/Noise/Polynomial.h"

// Include the math functions.
#include <cmath>

// ComputePolynomial definition for the constant polynomial.
template<>
double UPolynomialNoise::ComputePolynomial(UPolynomialNoise::Constant& factors, double x) {
    // Return the constant value.
    return factors.a;
}

// ComputePolynomial definition for the linear polynomial.
template<>
double UPolynomialNoise::ComputePolynomial(UPolynomialNoise::Linear& factors, double x) {
    // Return the linear computation.
    return factors.a + factors.b * x;
}

// ComputePolynomial definition for the quadratic polynomial.
template<>
double UPolynomialNoise::ComputePolynomial(UPolynomialNoise::Quadratic& factors, double x) {
    // Return the quadratic computation.
    return factors.a + factors.b * x + factors.c  * x * x;
}

// ComputePolynomial definition for the cubic polynomial.
template<>
double UPolynomialNoise::ComputePolynomial(UPolynomialNoise::Cubic& factors, double x) {
    // Return the cubic computation.
    return factors.a + factors.b * x + factors.c * x * x + factors.d * x * x * x;
}

// Creates the displacement factors given a set of parameters.
void UPolynomialNoise::GenerateDisplacementFactors(double height, double width, double originAngle, double extentAngle, UPolynomialNoise::Cubic& factors) {
    // Compute the factors of the polynomial.
    factors.a = height;
    factors.b = tan(originAngle * PI / 180);
    factors.c = - ((3 * height) / (width * width)) - ((2 * tan(originAngle * PI / 180)) / (width)) - ((tan(extentAngle * PI / 180)) / (width));
    factors.d = ((tan(originAngle * PI / 180) + tan(extentAngle * PI / 180)) / (width * width)) + ((2 * height) / (width * width * width));
}
