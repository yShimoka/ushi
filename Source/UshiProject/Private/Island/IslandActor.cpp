//
//  Actor.cpp
//  UshiProject_Index
//
//  Created by Jean-Baptiste CAILLAUD on 16/11/2019.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

// Include the actor header file.
#include "Island/IslandActor.h"
// Include the log file.
#include "Tools/Log.h"

#include "DrawDebugHelpers.h"
#include <cmath>
#include "Tools/Map/PointConverter.h"
#include "Island/IslandAsyncTasks.h"

// Define the constructor.
AIslandActor::AIslandActor() {
    // Allow the actor to tick.
	PrimaryActorTick.bCanEverTick = true;
    // Compute the tick after the physics update.
    PrimaryActorTick.TickGroup = TG_PostPhysics;
}

// Define the BeginPlay method.
void AIslandActor::BeginPlay() {
    // Call the parent BeginPlay method.`
    Super::BeginPlay();
    
    // Generate the noise map.
    _mpNoiseMap = new float[pow(Parameters.LatticeResolution, 2)];
    
    // Create the generator.
    UPerlinGenerator* generator = UPerlinGenerator::CreateGenerator(Parameters.Seed, this);
    
    // Create the semaphore.
    FGenericPlatformProcess::FSemaphore semaphore = new FGenericPlatformProcess::FSemaphore(TEXT("NoiseGenerator"));
    
    // Chunkify the tasks.
    for (int i = 0; i < pow(Parameters.ChunkCount, 2); i++) {
        // Prepare the task.
        UNoiseMapGeneratorTask* task = NewObject<UNoiseMapGeneratorTask>(this);
        task->Setup(_mpNoiseMap, &Parameters, i, generator, 0.f, semaphore);
        
        // Run the task.
        _mWorkers.push_back(UAsyncLibrary::RunTaskAsync(task));
    }
}

void DrawLine(UPointConverter::Point2D origin, UPointConverter::Point2D extent, size_t resolution, float nOrigin, float nExtent, UWorld* world) {
    // Generate the position vector.
    static const FVector2D CENTER = FVector2D(resolution / 2, resolution / 2);
    FVector vOrigin = FVector(CENTER - (FVector2D(origin.x, origin.y) / resolution * 1000), nOrigin);
    FVector vExtent = FVector(CENTER - (FVector2D(extent.x, extent.y) / resolution * 1000), nExtent);
    
    // Draw the line.
    DrawDebugLine(world, vOrigin, vExtent, FColor::Red, true);
}

// Define the Tick method.
void AIslandActor::Tick(float DeltaTime) {
    // Call the parent tick method.
    Super::Tick(DeltaTime);
    
    // If the list is empty, stop the method.
    if (_mWorkers.size() == 0) {
        return;
    }
    
    // Check if the generator is done.
    for (auto it: _mWorkers) {
        if (!it->IsDone()) {
            return;
        }
    }
    
    // Clear the workers array.
    _mWorkers.clear();
    
    // Draw the map.
    for (size_t y = 1; y < Parameters.LatticeResolution; y++)
    for (size_t x = 1; x < Parameters.LatticeResolution; x++) {
        // If the point is on the origin of the map.
        if (x == 1) {
            DrawLine(
                { x - 1, y - 1 },
                { x - 1, y - 0 },
                Parameters.LatticeResolution,
                _mpNoiseMap[UPointConverter::GetPointIndex<UPointConverter::Point2D>({ x - 1, y - 1 }, Parameters.LatticeResolution)],
                _mpNoiseMap[UPointConverter::GetPointIndex<UPointConverter::Point2D>({ x - 1, y - 0 }, Parameters.LatticeResolution)],
                GetWorld()
            );
        }
        // If the point is on the origin of the y axis.
        if (y == 1) {
            DrawLine(
                { x - 1, y - 1 },
                { x - 0, y - 1 },
                Parameters.LatticeResolution,
                _mpNoiseMap[UPointConverter::GetPointIndex<UPointConverter::Point2D>({ x - 1, y - 1 }, Parameters.LatticeResolution)],
                _mpNoiseMap[UPointConverter::GetPointIndex<UPointConverter::Point2D>({ x - 0, y - 1 }, Parameters.LatticeResolution)],
                GetWorld()
            );
        }
        
        DrawLine(
            { x - 0, y - 1 },
            { x - 0, y - 0 },
            Parameters.LatticeResolution,
            _mpNoiseMap[UPointConverter::GetPointIndex<UPointConverter::Point2D>({ x - 0, y - 1 }, Parameters.LatticeResolution)],
            _mpNoiseMap[UPointConverter::GetPointIndex<UPointConverter::Point2D>({ x - 0, y - 0 }, Parameters.LatticeResolution)],
            GetWorld()
        );
        DrawLine(
            { x - 1, y - 0 },
            { x - 0, y - 0 },
            Parameters.LatticeResolution,
            _mpNoiseMap[UPointConverter::GetPointIndex<UPointConverter::Point2D>({ x - 1, y - 0 }, Parameters.LatticeResolution)],
            _mpNoiseMap[UPointConverter::GetPointIndex<UPointConverter::Point2D>({ x - 0, y - 0 }, Parameters.LatticeResolution)],
            GetWorld()
        );
    }
}
