//
//  IslandAsyncTasks.h
//  UshiProject
//
//  Created by Jean-Baptiste CAILLAUD on 17/11/2019.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

#pragma once

// Include the asynchronous base classes.
#include "Tools/Async/AsyncLibrary.h"
// Include the perlin generator.
#include "Tools/Noise/Perlin.h"
// Include the island parameters.
#include "IslandParameters.h"

// Include the semaphore struct.
#include "GenericPlatform/GenericPlatformProcess.h"

// Include the generated header.
#include "IslandAsyncTasks.generated.h"

/**
 * AsyncTask class used to generate noise maps.
 * Allows the chunkification of the world generation.
 *
 * @author Caillaud Jean-Baptiste.
 */
UCLASS(BlueprintType)
class UNoiseMapGeneratorTask : public UAsyncTask {
    GENERATED_BODY()
    
    // ---  Attributes ---
        // -- Private Attributes --
            /** Pointer to the modified map object. */
            float* _mpMap;
            
            /** Generator parameters. */
            FIslandParameters* _mpParams;
            
            /** Assigned chunk identifier. */
            size_t _mChunkId;
            
            /** Perlin generator instance. */
            UPerlinGenerator* _mpGenerator;
            
            /** Slice of the generator used for this noise map. */
            float _mSlice;
            
            /** Semaphore dedicated to the map object. */
            FGenericPlatformProcess::FSemaphore _mSemaphore;
    // --- /Attributes ---
    
    // ---  Methods ---
        // -- Public Methods --
public:
            /**
             * Prepares the task before its execution.
             *
             * @param map The map object that will be altered.
             * @param parameters The island parameters used to build the island.
             * @param chunkId The index of the chunk to compute.
             * @param generator A pointer to the generator instance used.
             * @param slice The slice of the generator noise used in this generator.
             */
            void Setup(
                float* map,
                FIslandParameters* parameters,
                size_t chunkId,
                UPerlinGenerator* generator,
                float slice,
                FGenericPlatformProcess::FSemaphore semaphore
            );
        
            /**
             * Override of the runtask implementation.
             * Computes the noise map from the specified parameters.
             */
            void RunTask_Implementation() override;
    // --- /Methods ---
};
