//
//  Actor.h
//  UshiProject
//
//  Created by Jean-Baptiste CAILLAUD on 16/11/2019.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

#pragma once

// Include the actor base class.
#include "GameFramework/Actor.h"
// Include the vector class.
#include <vector>

// Include the island parameters.
#include "IslandParameters.h"
// Include the async library.
#include "Tools/Async/AsyncLibrary.h"

// Include the generated header.
#include "IslandActor.generated.h"

UCLASS()
class AIslandActor : public AActor {
    GENERATED_BODY()
    
    // ---  Attributes ---
        // -- Public Attributes --
public:
            /** The parameters of the generated island. */
            UPROPERTY(EditAnywhere, Category = "Generator")
            FIslandParameters Parameters;
        
        // -- Private Attributes --
private:
            std::vector<TSharedPtr<UAsyncLibrary::Worker>> _mWorkers;
            float* _mpNoiseMap;
    // --- /Attributes ---
    
    // ---  Methods ---
        // -- Constructor --
public:
            /**
             * Generates a new island actor object.
             * Sets the actor object up.
             */
            AIslandActor();
            
        // -- Public Methods --
        
            /**
             * BeginPlay method of the actor.
             * This method gets called once the world is up and ready to be played.
             */
            void BeginPlay() override;
            
            /**
             * Ticking method of the actor.
             * This method gets called after every physics computation frame.
             *
             * @param DeltaTime The time elapsed since the last Tick call.
             */
            void Tick(float DeltaTime) override;
    // --- /Methods ---
};
