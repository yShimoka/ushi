//
//  Log.h
//  UshiProject
//
//  Created by Jean-Baptiste CAILLAUD on 16/11/2019.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

#pragma once

// Include the engine's minimal declaration.
#include "CoreMinimal.h"

// Define the UshiLog logging category.
DECLARE_LOG_CATEGORY_EXTERN(UshiLog, Verbose, All)
