//
//  PointConverter.h
//  UshiProject
//
//  Created by Jean-Baptiste CAILLAUD on 17/11/2019.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

#pragma once

// Include the blueprint funciton library header.
#include "Kismet/BlueprintFunctionLibrary.h"

// Include the generated header.
#include "PointConverter.generated.h"

/**
 * Map point converter helper class.
 * Computes helpful parameters to and from 2D or 3D map points.
 *
 * @author Caillaud Jean-Baptiste.
 */
UCLASS()
class UPointConverter : public UBlueprintFunctionLibrary {
    GENERATED_BODY()
    
    // ---  SubObjects --
        // -- Public Structs --
public:
            /**
             * Structure defining the contents of a 2D point.
             */
            struct Point2D { size_t x, y; };
            
            /**
             * Structure defining the contents of a 3D point.
             */
            struct Point3D { size_t x, y, z; };
            
        // -- Public Typedefs --
            /**
             * Type alias for a point index in a buffer.
             */
            typedef size_t PointIndex;
            
            /**
             * Type alias for a chunk index in a buffer.
             */
            typedef size_t ChunkIndex;
    // --- /SubObjects --
    
    // ---  Methods ---
        // -- Public Methods --
public:
            /**
             * Returns the Point index from the specified point.
             *
             * @tparam TPoint The type of point to get the index for,
             * @param point The point to get the index of.
             * @param width The size of the map.
             * @returns The index of the specified point.
             */
            template<typename TPoint>
            static PointIndex GetPointIndex(TPoint point, size_t width);
            
            /**
             * Returns the Point from the specified index.
             *
             * @tparam TPoint The type of point to parse the index of.
             * @param point The requested index.
             * @param width The size of the map.
             * @returns The coordinates of the specified point.
             */
            template<typename TPoint>
            static TPoint GetPoint(PointIndex point, size_t width);
            
            /**
             * Returns the Point from the specified chunk.
             *
             * @tparam TPoint The type of point to parse the index of.
             * @param chunk The requested chunk's index.
             * @param width The size of the map.
             * @param chunkCount The number of chunks in the map.
             * @returns The coordinates of the specified point.
             */
            template<typename TPoint>
            static TPoint GetChunkPoint(ChunkIndex chunk, size_t width, size_t chunkCount);
    // --- /Methods ---
};
