//
//  AsyncLibrary.h
//  UshiProject
//
//  Created by Jean-Baptiste CAILLAUD on 17/11/2019.
//  Copyright © 2019 Epic Games, Inc. All rights reserved.
//

#pragma once

// Include the asynchronous classes.
#include "Async/AsyncWork.h"
// Include the Blueprint function library base class.
#include "Kismet/BlueprintFunctionLibrary.h"
// Include the shared pointer template object.
#include "Templates/SharedPointer.h"
// Include the unique pointer template object.
#include "Templates/UniquePtr.h"

// Include the generated header.
#include "AsyncLibrary.generated.h"

/**
 * Base class used for asynchronous tasks.
 *
 * @author Caillaud Jean-Baptiste.
 */
UCLASS(BlueprintType, Blueprintable)
class UAsyncTask : public UObject {
    GENERATED_BODY()
    
    // ---  Methods ---
        // -- Public Methods --
public:
            /**
             * Run task method.
             * Can be overriden in Blueprint subclasses.
             * @see RunTask_Implementation for implementation informations.
             */
            UFUNCTION(BlueprintNativeEvent, Category = "Task")
            void RunTask();
            
            /**
             * C++ implememtation of the RunTask method.
             * Expected to be overriden in child classes.
             */
            virtual void RunTask_Implementation();
    // --- /Methods ---
};

/**
 * Asynchronous worker task.
 * Gets executed on a worker thread and runs a UAsyncTask object.
 * Created by the UAsyncLibrary object.
 *
 * @author Caillaud Jean-Baptiste.
 */
class FAsyncWorker : public FNonAbandonableTask {
    friend class FAsyncTask<FAsyncWorker>;
    friend class FAutoDeleteAsyncTask<FAsyncWorker>;
    
    // ---  Attributes ---
        // -- Private Attributes --
private:
            /** Unique pointer to the task that must be run by this worker. */
            UAsyncTask* _mpTask;
    // --- /Attributes ---
    
    // ---  Methods ---
        // -- Constructor --
public:
            /**
             * Class constructor.
             * Instantiates a new worker with the specified task object.
             *
             * @param task The task to run on this worker.
             */
            FAsyncWorker(UAsyncTask* task);
        
        // -- Private Methods --
private:
            /**
             * Executes the worker.
             * Calls the task's RunTask method.
             */
            void DoWork();
            
            /** UE4-required method. */
            FORCEINLINE TStatId GetStatId() const { RETURN_QUICK_DECLARE_CYCLE_STAT(FAsyncWorker, STATGROUP_ThreadPoolAsyncTasks); }
    // --- /Methods ---
};

/**
 * Library class used to run tasks.
 * This object takes UAsyncTasks descriptions and creates FAsyncWorker instances.
 * The workers get dispatched to the worker pool and run asynchronously.
 *
 * @author Caillaud Jean-Baptiste.
 */
UCLASS()
class UAsyncLibrary : public UBlueprintFunctionLibrary {
    GENERATED_BODY()
    
    // ---  SuboObjects ---
        // -- Public Typedefs --
public:
            // Type alias for the returned worker instance.
            typedef FAsyncTask<FAsyncWorker> Worker;
    // --- /SuboObjects ---
    
    // ---  Methods ---
        // -- Public Methods --
public:
            /**
             * Runs the task asynchronously, creating a new Worker instance.
             * This method takes ownership of the task object.
             *
             * @param task The task object to run.
             * @returns A shared pointer to the new worker instance.
             */
            static TSharedPtr<Worker> RunTaskAsync(UAsyncTask* task);
    // --- /Methods ---
};
