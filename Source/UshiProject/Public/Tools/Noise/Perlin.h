//
//  Perlin.h
//  UshiProject
//
//  Created by Jean-Baptiste CAILLAUD on 16/11/2019.
//  Copyright © 2019 Ludus Académie. All rights reserved.
//

#pragma once

// Include the Engine's minimal requirements.
#include "CoreMinimal.h"
// Include the shared pointer template.
#include "Templates/SharedPointer.h"

// Include the generated header.
#include "Perlin.generated.h"

/**
 * Perlin noise generator class.
 * Generates pseudo-random noise based on Perlin's algorithm.
 *
 * @author Caillaud Jean-Baptiste.
 */
UCLASS(BlueprintType)
class UPerlinGenerator : public UObject {
    GENERATED_BODY()

    // ---  Attributes ---
        // -- Private Attributes --
private:
            /**
             * Permutation vector used to generate the perlin noise.
             * Initialized with the class constructor, immutable afterwards.
             * Shared accross all copies of the generator instance.
             */
            TSharedPtr<const unsigned char> _mpPermutationBuffer;
    // --- /Attributes ---
    
    // ---  Methods ---
        // -- Constructor --
public:
            /**
             * Empty constructor.
             * Creates a new perlin generator instance with no permutation buffer.
             */
            UPerlinGenerator();
        // -- Public Methods --
public:
            /**
             * Creates a new Perlin noise generator instance.
             * Uses the seed passed as a paramter for the random engine.
             *
             * @param seed The seed passed to the random engine.
             * @param owner The instance owning the generator.
             * @returns A new instance of a PerlinGenerator.
             */
            static UPerlinGenerator* CreateGenerator(int seed, UObject* owner);
            
            /**
             * Returns the noise value at the requested point.
             * The value will be normalized between 0 and 1.
             *
             * @param x The x coordinate of the requested point.
             * @param y The y coordinate of the requested point.
             * @param z The z coordinate of the requested point.
             * @returns The noise value for the requested point.
             */
            UFUNCTION(BlueprintPure, Category = "Noise")
            const float GetNoiseAt(float x, float y, float z) const;
        // -- Private Methods --
private:
            /**
             * Computes the fade curve of the specified value.
             *
             * @param t The value to compute the fade curve of.
             */
            inline const float _Fade(float t) const;
            
            /**
             * Computes the linear interpolation of the value t between a and b.
             *
             * @param t The value to interpolate for.
             * @param a The origin of the interpolation line (aka. f(0) = a).
             * @param b The destination of the interpolation line (aka. f(1) = b).
             * @returns The value of the interpolation (aka. f(t)).
             */
            inline const float _Lerp(float t, float a, float b) const;
            inline const float _Grad(int hash, float x, float y, float z) const;
    // --- /Methods ---
};
